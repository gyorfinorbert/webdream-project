<?php
/**
 * Created by PhpStorm.
 * User: gyorfinorbert
 * Date: 2018. 08. 30.
 * Time: 7:32
 */

class Brand
{
    private $name;
    private $quality;

    const ERRORS = [
        0 => 'The @property name cannot be empty.',
        1 => 'The @property quality must be a value from 1 to 5.'
    ];


    public function __construct(string $name, int $quality) {
        if (empty($name)) {
            throw new Exception(self::ERRORS[0], 0);
        }
        if ($quality <= 0 || $quality > 5) {
            throw new Exception(self::ERRORS[1], 1);
        }

        $this->name = $name;
        $this->quality = $quality;
    }

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->{$property};
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            switch ($property) {
                case 'name':
                    if (empty($value)) {
                        throw new Exception(self::ERRORS[0], 0);
                    }

                    $this->{$property} = (string)$value;
                    break;
                case 'quality':
                    if ((int)$value <= 0 || (int)$value > 5) {
                        throw new Exception(self::ERRORS[1], 1);
                    }

                    $this->{$property} = (int)$value;
                    break;
            }
        }

        return $this;
    }

    public function __toString() : string {
        return 'Brand';
    }
}
