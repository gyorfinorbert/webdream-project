<?php
/**
 * Created by PhpStorm.
 * User: gyorfinorbert
 * Date: 2018. 08. 30.
 * Time: 7:30
 */

class Warehouse
{
    private $name;
    private $address;
    private $capacity;
    private $products = [];
    private $used_capacity = 0;

    const ERRORS = [
        0 => 'The @property name or @property address cannot be empty.',
        1 => 'The value of @property capacity must be a positive integer.',
        2 => 'The value of @property products is invalid.',
        3 => 'The value of @parameter amount must be a positive integer.',
        4 => 'There is no any product with the given ID.',
        5 => 'The given amount of item is more than available in the warehouse.',
        6 => 'Unknown error occurred.'
    ];


    /*
     * The structure of @property products must match the following:
       [
           [
               'amount' => int x,
               'product' => Product obj1
           ],
           [
               'amount' => int y,
               'product' => Product obj2
           ],
           ...
       ]
    */
    public function __construct(string $name, string $address, int $capacity, array $products = null) {
        if (empty($name) || empty($address)) {
            throw new Exception(self::ERRORS[0], 0);
        }
        if ($capacity < 1) {
            throw new Exception(self::ERRORS[1], 1);
        }
        if (is_array($products) && self::validateProducts($products) === false) {
            throw new Exception(self::ERRORS[2], 2);
        }

        $this->name = $name;
        $this->address = $address;
        $this->capacity = $capacity;

        if (is_array($products)) {
            $this->setProducts($products);
        }
    }

    public function __get($property) {
        if (property_exists($this, $property)) {
            switch ($property) {
                case 'name': case 'address': case 'capacity': case 'products':
                    return $this->{$property};
            }
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            switch ($property) {
                case 'name':
                case 'address':
                    if (empty($value)) {
                        throw new Exception(self::ERRORS[0], 0);
                    }

                    $this->{$property} = (string)$value;
                    break;
                case 'capacity':
                    if ((int)$value < 1) {
                        throw new Exception(self::ERRORS[1], 1);
                    }

                    $this->{$property} = (int)$value;
                    break;
                case 'products':
                    if (self::validateProducts($value) === false) {
                        throw new Exception(self::ERRORS[2], 2);
                    }

                    $this->setProducts($value);
                    break;
            }
        }

        return $this;
    }

    public function __toString() : string {
        $return = '';
        $return .= '<p><b>Warehouse [' . $this->name . ']</b></p>' . "\n";
        $return .= '<p>Address: ' . $this->address . '</p>' . "\n";
        $return .= '<p>Capacity: ' . $this->capacity . '</p>' . "\n";
        $return .= '<p>In stock:</p>' . "\n";
        $return .= '<table>' . "\n";
        $return .= '<tr>' . "\n";
        $return .= '<th>Item ID</th>' . "\n";
        $return .= '<th>Name</th>' . "\n";
        $return .= '<th>Price</th>' . "\n";
        $return .= '<th>Brand</th>' . "\n";
        $return .= '<th>AMOUNT</th>' . "\n";
        $return .= '</tr>' . "\n";

        foreach ($this->products as $product) {
            $return .= '<tr><td>' . $product['product']->item_id . '</td><td>'
                . $product['product']->name . '</td><td>' . $product['product']->price . '</td><td>'
                . $product['product']->brand->name . '</td><td>' . $product['amount'] . '</td></tr>' . "\n";
        }

        $return .= '</table>' . "\n";

        return $return;
    }

    protected static function validateProducts($products) : bool {
        if (!is_array($products)) {
            return false;
        }

        foreach ($products as $product) {
            if (!is_array($product)
                || !array_key_exists('amount', $product) || !is_int($product['amount']) || $product['amount'] < 1
                || !array_key_exists('product', $product) || !$product['product'] instanceof Product) {
                return false;
            }
        }

        return true;
    }

    protected function setProducts($products) : array {
        if ($this->capacity <= $this->used_capacity) {
            return $products;
        }

        foreach ($products as $key => $product) {
            if (!array_key_exists($product['product']->item_id, $this->products)) {
                $capacity_left = $this->capacity - $this->used_capacity;

                if ($capacity_left <= 0) {
                    break;
                }

                if ($capacity_left >= $product['amount']) {
                    $this->products[$product['product']->item_id] = [
                        'amount' => $product['amount'],
                        'product' => $product['product']
                    ];
                    $this->used_capacity += $product['amount'];

                    unset($products[$key]);
                } else {
                    $this->products[$product['product']->item_id] = [
                        'amount' => $capacity_left,
                        'product' => $product['product']
                    ];
                    $this->used_capacity += $capacity_left;

                    $products[$key]['amount'] -= $capacity_left;
                }
            }
        }

        return $products;
    }


    public function addProduct(Product $product, int $amount) : array {
        if ($amount < 1) {
            throw new Exception(self::ERRORS[3], 3);
        }

        $p = $this->setProducts([
            [
                'amount' => $amount,
                'product' => $product
            ]
        ]);

        reset($p);

        return (!empty($p)) ? $p[key($p)] : [];
    }

    public function addAmount(string $item_id, int $amount) : array {
        if (!array_key_exists($item_id, $this->products)) {
            throw new Exception(self::ERRORS[4], 4);
        }

        $p = [];

        $next_capacity = $this->used_capacity + $amount;
        $next_product_amount = $this->products[$item_id]['amount'] + $amount;

        if ($next_capacity <= $this->capacity && $next_product_amount > 0) {
            $this->products[$item_id]['amount'] += $amount;
            $this->used_capacity += $amount;
        } else if ($next_capacity > $this->capacity && $next_product_amount > 0) {
            $capacity_left = $this->capacity - $this->used_capacity;

            $this->products[$item_id]['amount'] += $capacity_left;
            $this->used_capacity += $capacity_left;

            $p = [
                'amount' => $amount - $capacity_left,
                'product' => $this->products[$item_id]['product']
            ];
        } else if ($next_product_amount === 0) {
            unset($this->products[$item_id]);
            $this->used_capacity += $amount;
        } else if ($next_product_amount < 0) {
            throw new Exception(self::ERRORS[5], 5);
        } else {
            throw new Exception(self::ERRORS[6], 6);
        }

        return $p;
    }
}
