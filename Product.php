<?php
/**
 * Created by PhpStorm.
 * User: gyorfinorbert
 * Date: 2018. 08. 30.
 * Time: 7:33
 */

class Product
{
    protected $item_id;
    protected $name;
    protected $price;
    protected $brand;

    const ERRORS = [
        0 => 'The @property item_id or @property name cannot be empty.',
        1 => 'The @property brand must be a Brand type.'
    ];


    public function __construct(string $item_id, string $name, float $price, Brand $brand) {
        if (empty($item_id) || empty($name)) {
            throw new Exception(self::ERRORS[0], 0);
        }

        $this->item_id = trim($item_id);
        $this->name = $name;
        $this->price = $price;
        $this->brand = $brand;
    }

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->{$property};
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            switch ($property) {
                case 'item_id':
                case 'name':
                    if (empty($value)) {
                        throw new Exception(self::ERRORS[0], 0);
                    }

                    $this->{$property} = (string)$value;
                    break;
                case 'price':
                    $this->{$property} = (float)$value;
                    break;
                case 'brand':
                    if (!$value instanceof Brand) {
                        throw new Exception(self::ERRORS[1], 1);
                    }

                    $this->{$property} = $value;
                    break;
            }
        }

        return $this;
    }

    public function __toString() : string {
        return 'Product';
    }
}
