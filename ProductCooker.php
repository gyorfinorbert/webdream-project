<?php
/**
 * Created by PhpStorm.
 * User: gyorfinorbert
 * Date: 2018. 08. 30.
 * Time: 7:33
 */

class ProductCooker extends Product
{
    private $type;

    const ERRORS = [
        0 => 'The value of @property type is invalid.'
    ];

    const TYPE = [
        'electric',
        'gas'
    ];


    public function __construct(string $item_id, string $name, float $price, Brand $brand, string $type) {
        parent::__construct($item_id, $name, $price, $brand);

        if (!in_array($type, self::TYPE)) {
            throw new Exception(self::ERRORS[0], 0);
        }

        $this->type = $type;
    }

    public function __set($property, $value) {
        parent::__set($property, $value);

        if (property_exists($this, $property)) {
            switch ($property) {
                case 'type':
                    if (!in_array($value, self::TYPE)) {
                        throw new Exception(self::ERRORS[0], 0);
                    }

                    $this->{$property} = (string)$value;
                    break;
            }
        }

        return $this;
    }

    public function __toString() : string {
        return 'ProductCooker';
    }
}
