<?php
/**
 * Created by PhpStorm.
 * User: gyorfinorbert
 * Date: 2018. 08. 30.
 * Time: 7:33
 */

class ProductLawnmower extends Product
{
    private $tank_capacity;


    public function __construct(string $item_id, string $name, float $price, Brand $brand, int $tank_capacity) {
        parent::__construct($item_id, $name, $price, $brand);

        $this->tank_capacity = $tank_capacity;
    }

    public function __set($property, $value) {
        parent::__set($property, $value);

        if (property_exists($this, $property)) {
            switch ($property) {
                case 'tank_capacity':
                    $this->{$property} = (int)$value;
                    break;
            }
        }

        return $this;
    }

    public function __toString() : string {
        return 'ProductLawnmower';
    }
}
