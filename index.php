<?php
spl_autoload_register(function ($class_name) {
    if (file_exists('./' . $class_name . '.php')) {
        require_once('./' . $class_name . '.php');
    }
});

/*
 * A. CASE
 */
try {
    // WAREHOUSE A1
    $wh_a1 = new Warehouse(
        'Warehouse A1',
        '1118 Bp. Melinda u. 1.',
        100,
        array(
            array(
                'amount' => 10,
                'product' => new Product(
                    'a0001',
                    'OLED55B7V',
                    429900.0,
                    new Brand('LG', 5)
                )
            )
        )
    );
    $wh_a1->addProduct(new Product(
        'a0002',
        'Mac Pro',
        1469990.0,
        new Brand('Apple', 5)
    ), 20);

    // WAREHOUSE A2
    $wh_a2 = new Warehouse(
        'Warehouse A2',
        '3500 Miskolc, Petőfi Sándor u. 1.',
        50
    );
    $wh_a2->addProduct(new ProductCooker(
        'a0003',
        'ZCG210L1WA',
        47900.0,
        new Brand('Zanussi', 4),
        'gas'
    ), 20);
    $wh_a2->addProduct(new ProductLawnmower(
        'a0004',
        'LC356AWD',
        239900.0,
        new Brand('Husqvarna', 5),
        2
    ), 30);
} catch (Exception $e) {
    die($e->getMessage());
}

/*
 * B. CASE
 */
try {
    // WAREHOUSE B1
    $wh_b1 = new Warehouse(
        'Warehouse B1',
        '1118 Bp. Melinda u. 1.',
        100
    );

    $remain_b1 = [];
    $remain_b1[] = $wh_b1->addProduct(new Product(
        'a0001',
        'OLED55B7V',
        429900.0,
        new Brand('LG', 5)
    ), 30);
    $remain_b1[] = $wh_b1->addProduct(new Product(
        'a0002',
        'Mac Pro',
        1469990.0,
        new Brand('Apple', 5)
    ), 80);

    // WAREHOUSE B2
    $wh_b2 = new Warehouse(
        'Warehouse B2',
        '3500 Miskolc, Petőfi Sándor u. 1.',
        50
    );
    $wh_b2->addProduct(new ProductCooker(
        'a0003',
        'ZCG210L1WA',
        47900.0,
        new Brand('Zanussi', 4),
        'gas'
    ), 20);
    $wh_b2->addProduct(new ProductLawnmower(
        'a0004',
        'LC356AWD',
        239900.0,
        new Brand('Husqvarna', 5),
        2
    ), 10);

    if (!empty($remain_b1)) {
        foreach ($remain_b1 as $p) {
            if (!empty($p)) {
                $wh_b2->addProduct($p['product'], $p['amount']);
            }
        }
    }
} catch (Exception $e) {
    die($e->getMessage());
}

/*
 * C. CASE
 */
try {
    // WAREHOUSE C1
    $wh_c1 = new Warehouse(
        'Warehouse C1',
        '1118 Bp. Melinda u. 1.',
        100
    );

    $wh_c1->addProduct(new Product(
        'a0001',
        'OLED55B7V',
        429900.0,
        new Brand('LG', 5)
    ), 30);
    $wh_c1->addProduct(new Product(
        'a0002',
        'Mac Pro',
        1469990.0,
        new Brand('Apple', 5)
    ), 60);

    // WAREHOUSE C2
    $wh_c2 = new Warehouse(
        'Warehouse C2',
        '3500 Miskolc, Petőfi Sándor u. 1.',
        50
    );
    $wh_c2->addProduct(new ProductCooker(
        'a0003',
        'ZCG210L1WA',
        47900.0,
        new Brand('Zanussi', 4),
        'gas'
    ), 20);
    $wh_c2->addProduct(new ProductLawnmower(
        'a0004',
        'LC356AWD',
        239900.0,
        new Brand('Husqvarna', 5),
        2
    ), 10);
} catch (Exception $e) {
    die($e->getMessage());
}
?>
<!doctype html>
<html lang="hu">
<head>
    <meta charset="utf-8">
    <title>Warehouse App</title>
    <meta name="description" content="">
    <meta name="author" content="gyorfinorbert">
    <style type="text/css">
        table {
            border-collapse: collapse;
        }

        table th, table td {
            border: 1px solid #666;
            padding: 5px;
        }

        b, .error {
            color: red;
        }
    </style>
</head>
<body>
    <h2>A) CASE</h2>
    <?= $wh_a1; ?>
    <?= $wh_a2; ?>
    <h4><u>Asking for <b>2</b> OLED55B7V from <?= $wh_a1->name; ?> and <b>12</b> LC356AWD from <?= $wh_a2->name; ?>:</u></h4>
    <?php
    try {
        $wh_a1->addAmount('a0001', -2);
        $wh_a2->addAmount('a0004', -12);
    } catch (Exception $e) {
        echo '<p class="error">' . $e->getMessage() . '</p>';
    }
    ?>
    <?= $wh_a1; ?>
    <?= $wh_a2; ?>
    <hr>

    <h2>B) CASE</h2>
    <?= $wh_b1; ?>
    <?= $wh_b2; ?>
    <hr>

    <h2>C) CASE</h2>
    <?= $wh_c1; ?>
    <?= $wh_c2; ?>
    <?php
    try {
        $wh_c2->addAmount('a0004', -11);
    } catch (Exception $e) {
        echo '<p class="error">' . $e->getMessage() . '</p>';
    }
    ?>
    <?= $wh_c1; ?>
    <?= $wh_c2; ?>
</body>
</html>
